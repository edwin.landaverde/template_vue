import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Dashboard.vue'

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: () => import('../views/layout/layoutMain.vue'),
      children: [
        {
          path: '',
          name: 'Dashboard',
          component: Home,
        },  
      ],
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import('../views/Login.vue'),
    },
  ],
})
