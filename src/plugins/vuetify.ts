// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'

const light = {
  dark: false,
  colors: {
    background: '#F4F7FD',
    primary: '#313945',
    secondary: '#111C4E',
  }
}

const dark = {
  dark: true,
  colors: {
    primary: '#313945',
    secondary: '#111C4E'
  }
}


export default createVuetify({
  theme: {
    defaultTheme: 'light',
    themes: {
      light,
      dark,
    },
  },
})

