import { defineStore } from 'pinia'
import themeServices from '@/services/theme.services'

export const useDashboardStore = defineStore('dashboard', {
  state: () => ({ 
    drawer: false,
    mini: false,
    theme: 'light',
    modo_oscuro: false,
  }),
  actions: {
    setDrawer(drawer){
      this.drawer = drawer
    },
    setMini(mini){
      this.mini = mini 
    },
    guardarTema(tema){
      themeServices.guardarTema(tema)
      this.theme = tema
      this.modo_oscuro = tema === 'dark'
    },
    obtenerTema(){
      this.theme = themeServices.obtenerTema()
      this.modo_oscuro = this.theme === 'dark'
      return this.theme
    },
  },
})
