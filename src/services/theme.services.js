export const obtenerTema = () => {
    const tema = localStorage.getItem('tema')
    return tema ?? 'light'
  }
  
  export const guardarTema = (tema) => {
    if(tema !== 'light' && tema !== 'dark'){
      throw new Error('Tema no válido')
    }
    localStorage.setItem('tema', tema)
  }
  
  export const intercambiarTema = () => {
    const tema = obtenerTema()
    if(tema === 'light'){
      guardarTema('dark')
    }else if(tema === 'dark'){
      guardarTema('light')
    }else{
      throw new Error('Tema no válido')
    }
  }
  
  export default {
    obtenerTema,
    guardarTema,
    intercambiarTema,
  }
  